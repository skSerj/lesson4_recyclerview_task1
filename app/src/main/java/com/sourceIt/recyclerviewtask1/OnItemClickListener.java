package com.sourceIt.recyclerviewtask1;

public interface OnItemClickListener {
    void onItemClick (Smartphones smartphones);
}
