package com.sourceIt.recyclerviewtask1;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


public class ListFragment extends Fragment implements OnItemClickListener{

    private RecyclerView recyclerView;
    private SmartphoneAdapter smartphoneAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.my_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        smartphoneAdapter = new SmartphoneAdapter(root.getContext(),Generator.generate(), this);
        recyclerView.setAdapter(smartphoneAdapter);
        return root;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onItemClick(Smartphones smartphones) {
        new AlertDialog.Builder(getContext())
                .setTitle(String.valueOf(smartphones.getManufacturerAndModel()))
                .setMessage(String.format("Диагональ: %.1f ' ,\n" +
                        "память: %.1f GB,\n" +
                        "RAM: %d GB,\n" +
                        "price: %d UAH ", smartphones.getScreenDiagonal(), smartphones.getMemory(),smartphones.getRAM(),smartphones.getPrice()))
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
