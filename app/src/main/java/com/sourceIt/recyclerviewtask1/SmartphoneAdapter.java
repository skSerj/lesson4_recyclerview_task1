package com.sourceIt.recyclerviewtask1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SmartphoneAdapter extends RecyclerView.Adapter<SmartphoneAdapter.SmartphoneViewHolder> {

    private Context context;
    private List<Smartphones> smartphones;
    private OnItemClickListener listener;

    public SmartphoneAdapter(Context context, List<Smartphones> smartphones, OnItemClickListener listener) {
        this.context = context;
        this.smartphones = smartphones;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SmartphoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_smartphones, parent, false);
        return new SmartphoneViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull SmartphoneViewHolder holder, int position) {
        holder.bind(smartphones.get(position));
    }

    @Override
    public int getItemCount() {
        return smartphones.size();
    }

    static class SmartphoneViewHolder extends RecyclerView.ViewHolder {
        TextView smartphoneManufacturer;
        View root;
        OnItemClickListener listener;

        public SmartphoneViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;

            root = itemView.findViewById(R.id.root);
            smartphoneManufacturer = itemView.findViewById(R.id.txt_smartphone_manufacture);
        }

        public void bind(final Smartphones smartphones) {
            smartphoneManufacturer.setText(smartphones.getManufacturerAndModel());
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(smartphones);
                }
            });
        }
    }
}

