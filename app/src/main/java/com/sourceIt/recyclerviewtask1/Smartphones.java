package com.sourceIt.recyclerviewtask1;

public class Smartphones {
    private String ManufacturerAndModel;
    private float screenDiagonal;
    private int RAM;
    private float memory;
    private int price;

    public Smartphones(String manufacturerAndModel, float screenDiagonal, int RAM, float memory, int price) {
        ManufacturerAndModel = manufacturerAndModel;
        this.screenDiagonal = screenDiagonal;
        this.RAM = RAM;
        this.memory = memory;
        this.price = price;
    }

    public String getManufacturerAndModel() {
        return ManufacturerAndModel;
    }

    public float getScreenDiagonal() {
        return screenDiagonal;
    }

    public int getRAM() {
        return RAM;
    }

    public float getMemory() {
        return memory;
    }

    public int getPrice() {
        return price;
    }
}

