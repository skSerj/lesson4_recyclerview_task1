package com.sourceIt.recyclerviewtask1;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<Smartphones> generate() {
        ArrayList<Smartphones> list = new ArrayList<>();
        list.add(new Smartphones("Samsung S10e", 5.2f, 6, 32, 16900));
        list.add(new Smartphones("Samsung S10", 5.8f, 8, 64, 19800));
        list.add(new Smartphones("Samsung S10+", 6.1f, 8, 128, 23999));
        list.add(new Smartphones("Samsung S20", 5.8f, 8, 64, 26200));
        list.add(new Smartphones("Samsung S20+", 6.2f, 8, 128, 29000));
        list.add(new Smartphones("Samsung S20 Ultra", 6.4f, 10, 256, 40000));
        list.add(new Smartphones("Samsung M31", 5.5f, 4, 32, 7100));
        list.add(new Smartphones("Xiaomi Redmi 8", 5.4f, 6, 64, 6999));
        list.add(new Smartphones("Xiaomi Redmi 8T", 5.6f, 8, 64, 9999));
        list.add(new Smartphones("Xiaomi Redmi 10Note", 6.4f, 8, 128, 16000));
        list.add(new Smartphones("Iphone SE 2", 4.7f, 3, 32, 14999));
        list.add(new Smartphones("Iphone 11", 5.0f, 4, 64, 16999));
        list.add(new Smartphones("Iphone 11 PRO", 5.5f, 6, 128, 27999));
        list.add(new Smartphones("Iphone XS", 5.5f, 4, 64, 22999));
        list.add(new Smartphones("Iphone XS+", 5.9f, 4, 128, 25999));

        return list;
    }
}

